package br.workshop.limite.api.model.dto;

public class CreateLimiteRequest {

    private String idConta;
    private TipoConta tipo;

    public String getIdConta() {
        return idConta;
    }

    public void setIdConta(String idConta) {
        this.idConta = idConta;
    }

    public TipoConta getTipo() {
        return tipo;
    }

    public void setTipo(TipoConta tipo) {
        this.tipo = tipo;
    }
}
