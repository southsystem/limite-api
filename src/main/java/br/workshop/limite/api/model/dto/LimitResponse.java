package br.workshop.limite.api.model.dto;

import br.workshop.limite.api.model.entity.Limit;

import java.math.BigDecimal;

public class LimitResponse {

    private String id;
    private BigDecimal limit;

    public LimitResponse(Limit limit) {
        this.id = limit.getId();
        this.limit = limit.getLimit();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public BigDecimal getLimit() {
        return limit;
    }

    public void setLimit(BigDecimal limit) {
        this.limit = limit;
    }
}
