package br.workshop.limite.api.model.dto;

public enum TipoConta {
    PF, PJ
}
