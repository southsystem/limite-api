package br.workshop.limite.api.service;

import br.workshop.limite.api.config.BrokerInput;
import br.workshop.limite.api.model.dto.CreateLimiteRequest;
import br.workshop.limite.api.model.dto.LimitResponse;
import br.workshop.limite.api.model.entity.Limit;
import br.workshop.limite.api.repository.LimitRepository;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class LimitService {

    private final LimitRepository repository;
    private final LimitRulesService rulesService;

    public LimitService(LimitRepository repository, LimitRulesService rulesService) {
        this.repository = repository;
        this.rulesService = rulesService;
    }

    public LimitResponse create(CreateLimiteRequest request) {
        BigDecimal limitValue = rulesService.getLimitByType(request.getTipo());
        Limit limit = new Limit(request, limitValue);

        return new LimitResponse(repository.save(limit));
    }

    @StreamListener(BrokerInput.createAccountEvent)
    public void createFromQueue(CreateLimiteRequest request) {
        create(request);
    }
}
