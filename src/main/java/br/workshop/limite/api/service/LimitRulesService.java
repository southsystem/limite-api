package br.workshop.limite.api.service;

import br.workshop.limite.api.model.dto.TipoConta;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class LimitRulesService {

    @Value("${limit.pf}")
    private BigDecimal limitPf;

    @Value("${limit.pj}")
    private BigDecimal limitPj;

    public BigDecimal getLimitByType(TipoConta tipoConta) {
        if (tipoConta == TipoConta.PJ) {
            return limitPj;
        }
        return limitPf;
    }
}
