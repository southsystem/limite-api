package br.workshop.limite.api.config;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.messaging.SubscribableChannel;

public interface BrokerInput {

    String createAccountEvent = "CREATE_ACCOUNT_EVENT";

    @Input(BrokerInput.createAccountEvent)
    SubscribableChannel getCreateAccountEvent();
}
