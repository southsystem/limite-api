package br.workshop.limite.api.repository;

import br.workshop.limite.api.model.entity.Limit;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LimitRepository extends PagingAndSortingRepository<Limit, String> {

}
