package br.workshop.limite.api.resource;

import br.workshop.limite.api.model.dto.CreateLimiteRequest;
import br.workshop.limite.api.model.dto.LimitResponse;
import br.workshop.limite.api.service.LimitService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/limite")
public class LimitResource {

    private final LimitService service;

    public LimitResource(LimitService service) {
        this.service = service;
    }

    @PostMapping
    public ResponseEntity<LimitResponse> create(@RequestBody @Valid CreateLimiteRequest
                                                        request) {
        return ResponseEntity.ok(service.create(request));
    }
}
